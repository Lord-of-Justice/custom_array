﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using ArrayLibrary;

namespace ArrayLibrary.Tests
{
    [TestFixture]
    public class CustomArrayTest
    {
        #region Low
        [Test()]
        public void Array_Get_ShouldReturnSameReference()
        {
            //arrange
            var array = new int[] { 1, 2, 3, 4 };
            int first = -5;
            //act
            var custom = new CustomArray<int>(first, array);
            //assert
            Assert.AreSame(array, custom.Array, message: "Array property works incorrectly");
        }
        [TestCase(-4, 5, -3, 10)]
        [TestCase(1, 7, 2, 4)]
        [TestCase(0, 3, 1, 9)]
        public void Indexer_GetElementAtPosition_ShouldReturnValue(int first, int length, int n, int element)
        {
            //Act
            var custom = new CustomArray<int>(first, length);
            custom[n] = element;
            //Assert
            Assert.AreEqual(element, custom[n], message: "Index get property works incorrectly");

        }
        [TestCase(-1, 6)]
        [TestCase(0, 7)]
        [TestCase(5, 2)]
        public void Length_Get_ShouldReturnCorrectValue(int first, int length)
        {
            // Act
            var custom = new CustomArray<string>(first, length);
            //Assert
            Assert.AreEqual(length, custom.Length, message: "Length get  property works incorrectly ");
        }
        [TestCase(-1, 5)]
        [TestCase(0, 11)]
        [TestCase(7, 3)]
        public void First_Get_ShouldReturnCorrectValue(int first, int length)
        {
            // Act
            var custom = new CustomArray<int>(first, length);

            //Assert
            Assert.AreEqual(first, custom.First, message: "First get property works incorrectly ");
        }
        [TestCase(-8, 4, -5)]
        [TestCase(0, 8, 7)]
        [TestCase(3, 5, 7)]
        public void Last_Get_ShouldReturnCorrectValue(int first, int length, int last)
        {
            //Act
            var custom = new CustomArray<bool>(first, length);

            //Assert
            Assert.AreEqual(last, custom.Last, message: "Last get property works incorrectly ");

        }
        [Test]
        public void CustomArrayProperties_Get_ShouldReturnCorrectValues()
        {
            //Arrange
            int first = 4;
            int length = 4;
            int last = 7;

            //Act
            var custom = new CustomArray<int>(first, 5, 7, 3, 7);
            //Assert
            Assert.AreEqual(first, custom.First, message: "First get property works incorrectly");
            Assert.AreEqual(length, custom.Length, message: "Length get property works incorrectly");
            Assert.AreEqual(last, custom.Last, message: "Last get property works incorrectly");

        }


        #endregion

        #region Advanced
        [Test]
        public void CreateCustomArray_WithNullArray_ShoulThrowNullReferenceException()
        {
            //arrange
            int first = 1;
            int[] array = null;
            var expectedEx = typeof(NullReferenceException);
            //act
            var actEx = Assert.Catch(() => new CustomArray<int>(first, array));
            //assert
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "CustomArray can't be created with null array");

        }

        [TestCase(0)]
        [TestCase(-8)]
        [TestCase(-4)]
        public void Length_SetElementLessThan0_ShouldThrowArgumentException(int length)
        {
            //Arrange
            int first = 3;
            var expectedEx = typeof(ArgumentException);
            //Act
            var actEx = Assert.Catch(() => { var custom = new CustomArray<int>(first, length); });
            //Assert
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "Set property in Length should throw ArgumentException in case of length parameter smaller or equal 0  ");

        }

        [TestCase(4, 6, 3)]
        [TestCase(0, 11, 11)]
        [TestCase(-8, 3, -5)]
        public void Indexer_SetElementOutOfRange_ShouldThrowIndexOutOfRangeException(int first, int length, int index)
        {
            //Arrange
            int value = -5;
            //Act
            var custom = new CustomArray<long>(first, length);
            var expectedEx = typeof(IndexOutOfRangeException);
            //Assert
            var actEx = Assert.Catch(() => custom[index] = value);
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "Indexer  should throw IndexOutOfRangeException ,if index parameter  out of array range");

        }

        [Test]
        public void CreateCustomArray_WithListWithoutParams_ShouldThrowIndexOutOfRangeException()
        {
            //Arrange
            int first = 4;
            List<int> list = new List<int>();
            var expectedEx = typeof(ArgumentNullException);
            //Act
            var actEx = Assert.Catch(() => { var custom = new CustomArray<int>(first, list); });
            //Arrange
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "CustomArray can't be created with list without elements ");

        }

        [Test]
        public void CreateCustomArray_WithNull_ShouldThrowArgumentNullException()
        {
            //Arrange
            int first = 1;
            List<int> list = null;
            var expectedEx = typeof(ArgumentNullException);
            //Act
            var actEx = Assert.Catch(() => { var custom = new CustomArray<int>(first, list); });
            //Arrange
            Assert.AreEqual(expectedEx, actEx.GetType(),
                message: "CustomArray can't be created with null list ");

        }


        [Test]
        public void GetEnumerator_OfListAndCustomArray_ShouldHaveEqualElements()
        {
            List<double> list = new List<double>()
            {
                5,10.6,14.88
            };

            int first = 7;
            CustomArray<double> array = new CustomArray<double>(first, list);

            var en2 = list.GetEnumerator();
            var en = array.GetEnumerator();
            for (int i = 0; i < array.Length; i++)
            {
                if (en.MoveNext() && en2.MoveNext())
                {
                    Assert.AreEqual(en.Current, en2.Current, message: "GetEnumerator works incorretly ");
                }
            }

        }
        #endregion

    }
}


