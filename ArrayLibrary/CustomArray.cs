﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ArrayLibrary
{
    public class CustomArray<T> : IEnumerable<T>
    {
        private int first, last, length;
        private readonly T[] arr;

        /// <summary>
        /// Should return first index of array
        /// </summary>
        public int First { 
            get { return first; }
            private set
            {
                first = value;
                last = first + Length - 1;
            }
        }

        /// <summary>
        /// Should return last index of array
        /// </summary>
        public int Last { 
            get { return last; }
        }

        /// <summary>
        /// Should return length of array
        /// <exception cref="ArgumentException">Thrown when value was smaller than 0</exception>
        /// </summary>
        public int Length { 
            get { return length; }
            private set 
            {
                if (value > 0)
                {
                    length = value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }

        /// <summary>
        /// Should return array 
        /// </summary>     
        public T[] Array { get { return arr; }  }

        /// <summary>
        /// Constructor with first index and length
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="length">Length</param>         
        public CustomArray(int first, int length)
        {
            Length = length;
            First = first;
            arr = new T[length];
        }

        /// <summary>
        /// Constructor with first index and collection  
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Collection</param>
        ///  <exception cref="ArgumentNullException">Thrown when list is null</exception>
        /// <exception cref="ArgumentNullException">Thrown when count is smaler than 0</exception>
        public CustomArray(int first, IEnumerable<T> list)
        {
            if (list != null)
            {
                if (list.Count() > 0)
                {
                    Length = list.Count();
                    First = first;
                    arr = list.ToArray();
                }
                else throw new ArgumentNullException();
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        /// <summary>
        /// Constructor with first index and params
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Params</param>
        public CustomArray(int first, params T[] list)
        {
            if (list != null)
            {
                Length = list.Length;
                First = first;
                arr = list;
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        /// <summary>
        /// Indexer with get and set  
        /// </summary>
        /// <param name="item">Int index</param>        
        /// <returns></returns>
        /// <exception cref="IndexOutOfRangeException">Thrown when index out of array range</exception>
        public T this[int item]
        {
            get
            {
                var edge = item - First;
                if (edge >= 0 && edge < Length)
                {
                    return arr[edge];
                }
                throw new IndexOutOfRangeException("Out of array");
            }
            set
            {
                var edge = item - First;
                if (edge >= 0 && edge < Length)
                {
                    arr[edge] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException("Out of array");
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < arr.Length; i++)
            {
                yield return arr[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

    }
    
}
